/******************************************************************************
 Atmel - C driver - twi driver - tests
   - myAVR MK3 board driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Two Wire Interface (TWI/I2C) driver tests.
 *
 * @file      test.h
 * @see       test.c
 * @author    Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef TEST_TWI_H
#define TEST_TWI_H

#include <avr/io.h>


#if TEST == 1
void test_twi_master_transmitter_port_extension(void);
#elif TEST == 2
void test_twi_master_transmitter_reset_mode(void);
#elif TEST == 3
void test_twi_master_transmitter_address_mode(void);
#elif TEST == 4
void test_twi_master_receiver_port_extension(void);
#elif TEST == 5
void test_twi_master_receiver_reset_mode(void);
#elif TEST == 6
void test_twi_master_receiver_address_mode(void);
#elif TEST == 7
void test_twi_slave_transmitter_port_extension(void);
#elif TEST == 8
void test_twi_slave_transmitter_reset_mode(void);
#elif TEST == 9
void test_twi_slave_transmitter_address_mode(void);
#elif TEST == 10
void test_twi_slave_receiver_port_extension(void);
#elif TEST == 11
void test_twi_slave_receiver_reset_mode(void);
#elif TEST == 12
void test_twi_slave_receiver_address_mode(void);
#elif TEST == 254
void example_twi_master_blink(void);
#elif TEST == 255
void example_twi_master_button(void);
#else
  #error Selected TEST function is not defined!
#endif // TEST


#endif // TEST_TWI_H

